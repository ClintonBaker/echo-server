import { AuthorType } from "./AuthorType.js";

export const commentTypeDefs = `#graphql
  ${AuthorType}

  type Comment {
    id: ID!
    post: ID!
    author: Author
    createdAt: String!
    text: String!
    parentComment: ID
    likes: [ID]
  }

  type MutateCommentResponse {
    success: Boolean!
    message: String
  }

  extend type Query {
    getComments(postId: ID!): [Comment]
  }

  extend type Mutation {
    createComment(text: String!, postId: ID!, parentComment: ID): MutateCommentResponse
    likeComment(commentId: ID!): MutateCommentResponse
  }
`;
