import { postTypeDefs } from "./postSchema.js";
import { commentTypeDefs } from "./commentSchema.js";

const baseTypeDefs = `
type Query {
  _empty: String
}

type Mutation {
  _empty: String
}
`;

export const typeDefs = [baseTypeDefs, postTypeDefs, commentTypeDefs];
