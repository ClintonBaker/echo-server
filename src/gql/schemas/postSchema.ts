import { AuthorType } from "./AuthorType.js";

export const postTypeDefs = `#graphql
  ${AuthorType}

  type Post {
    id: ID!
    createdAt: String!
    title: String
    text: String!
    tags: [String]
    comments: [Comment]
    reblogs: [ID]
    likes: [ID]
    author: Author
  }

  type MutatePostResponse {
    success: Boolean!
    message: String
  }

  extend type Query {
    getPost(id: ID!): Post,
    getTimeline(offset: Int, limit: Int): [Post]
  }

  extend type Mutation {
    createPost(title: String, text: String!, tags: [String]): MutatePostResponse
    likePost(postId: ID!): MutatePostResponse
  }
`;
