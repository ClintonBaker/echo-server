export const AuthorType = `#graphql
  type Author {
    id: ID!
    username: String!
  }
`;
