export const resolveAuthor = async (doc, args, { userLoader }) => {
  return await userLoader.load(doc.author);
};
