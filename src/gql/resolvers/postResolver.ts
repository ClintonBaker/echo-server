import Post from "../../models/Post.js";
import User from "../../models/User.js";
import { resolveAuthor } from "./common.js";

const resolveComments = async (post, args, { commentsLoader }) => {
  return await commentsLoader.loadMany(post.comments);
};

export const postResolver = {
  Post: {
    author: resolveAuthor,
    comments: resolveComments,
  },
  Query: {
    getPost: async (_, { id }) => {
      try {
        const post = await Post.findById(id);
        return post;
      } catch (error: any) {
        console.error(error);
        throw new Error("Error fetching post");
      }
    },
    getTimeline: async (_, { offset = 0, limit = 10 }, context) => {
      const user = await User.findById(context.user.id);

      if (!user) {
        throw new Error("User not found");
      }

      const paginatedPostsIds = user.posts.slice(offset, offset + limit);

      const posts = await Post.find({
        _id: { $in: paginatedPostsIds },
      }).sort({ createdAt: -1 });

      return posts;
    },
  },
  Mutation: {
    createPost: async (_, { title, text, tags = [] }, context) => {
      const newPost = new Post({
        title,
        text,
        tags,
        author: context.user.id,
      });

      const user = await User.findById(context.user.id);

      try {
        await newPost.save();
        user.posts.push(newPost.id);
        await user.save();
        return {
          success: true,
          message: "Post created successfully",
        };
      } catch (error: any) {
        console.error(error);
        throw new Error("Error fetching post");
      }
    },
    likePost: async (_, { postId }, context) => {
      const userId = context.user.id;
      try {
        const user = await User.findById(userId).select("likedPosts").lean();
        const hasLiked = user.likedPosts
          .map((id) => id.toString())
          .includes(postId);

        const updateUser = User.findByIdAndUpdate(userId, {
          [hasLiked ? "$pull" : "$addToSet"]: { likedPosts: postId },
        });
        const updatePost = Post.findByIdAndUpdate(postId, {
          [hasLiked ? "$pull" : "$addToSet"]: { likes: userId },
        });

        await Promise.all([updateUser, updatePost]);

        return {
          success: true,
          message: hasLiked
            ? "Post unliked successfully"
            : "Post liked successfully",
        };
      } catch (error) {
        console.error(error);
        throw new Error("Error updating like status of post");
      }
    },
  },
};
