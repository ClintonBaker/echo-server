import Comment from "../../models/Comment.js";
import Post from "../../models/Post.js";
import User from "../../models/User.js";

export const commentResolver = {
  Query: {
    getComments: async (_, { postId }, context) => {
      try {
        const { comments } = await Post.findById(postId)
          .populate("comments")
          .sort({ createdAt: -1 });
        return comments;
      } catch (error) {
        throw new Error("Error fetching comments");
      }
    },
  },
  Mutation: {
    createComment: async (_, { text, postId, parentComment }, context) => {
      try {
        const newComment = new Comment({
          text,
          post: postId,
          parentComment,
          author: context.user.id,
        });
        await newComment.save();

        const updateUser = User.findByIdAndUpdate(context.user.id, {
          $push: { comments: newComment._id },
        });

        const updatePost = Post.findByIdAndUpdate(postId, {
          $push: { comments: newComment._id },
        });

        await Promise.all([updateUser, updatePost]);

        return {
          success: true,
          message: "Comment created successfully",
        };
      } catch (error) {
        console.error(error);
        throw new Error("Error creating comment");
      }
    },
    likeComment: async (_, { commentId }, context) => {
      const userId = context.user.id;
      try {
        const user = await User.findById(userId).select("likedComments").lean();
        const hasLiked = user.likedComments.includes(commentId);

        const updateUser = User.findByIdAndUpdate(userId, {
          [hasLiked ? "$pull" : "$addToSet"]: { likedComments: commentId },
        });
        const updateComment = Comment.findByIdAndUpdate(commentId, {
          [hasLiked ? "$pull" : "$addToSet"]: { likes: userId },
        });

        await Promise.all([updateUser, updateComment]);

        return {
          success: true,
          message: hasLiked
            ? "Comment unliked successfully"
            : "Comment liked successfully",
        };
      } catch (error) {
        console.error(error);
        throw new Error("Error updating like status of comment");
      }
    },
  },
};
