import { postResolver } from "./postResolver.js";
import { commentResolver } from "./commentResolver.js";
import { resolveAuthor } from "./common.js";

export const resolvers = {
  Query: {
    ...postResolver.Query,
    ...commentResolver.Query,
  },
  Mutation: {
    ...postResolver.Mutation,
    ...commentResolver.Mutation,
  },
  Post: {
    ...postResolver.Post,
  },
  Comment: {
    author: resolveAuthor,
  },
};
