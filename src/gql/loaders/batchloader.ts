export const batchEntities = async (model, ids, sort = null) => {
  try {
    const entities = await model.find({ _id: { $in: ids } }).sort(sort || {});

    if (sort) {
      return entities;
    }

    const entityMap = entities.reduce((acc, entity) => {
      acc[entity.id] = entity;
      return acc;
    }, {});

    return ids.map((id) => entityMap[id] || null);
  } catch (error) {
    console.error(
      `Error fetching entities for model ${model.modelName}:`,
      error
    );
    throw error;
  }
};
