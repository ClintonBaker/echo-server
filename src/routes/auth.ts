import express from "express";
import jwt from "jsonwebtoken";
import passport from "passport";

import User, { UserI } from "../models/User.js";
import authenticateToken from "../utils/middleware.js";
import path from "path";
import fs from "fs";
import { __uploads } from "../utils/helpers.js";

const auth = express.Router();

auth.post("/register", async (req, res) => {
  const userData = req.body;
  const newUser = {
    username: userData.username,
    email: userData.email,
  };
  const avatarUrl = `https://api.dicebear.com/7.x/bottts/svg?seed=${userData.username}`;
  try {
    const avatarRes = await fetch(avatarUrl);
    const avatar = await avatarRes.text();

    const avatarPath = path.join(__uploads, userData.username, "avatar.svg");

    const dir = path.dirname(avatarPath);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
      fs.writeFileSync(avatarPath, avatar);
    }

    User.register(newUser, userData.password, (err: any, user: UserI) => {
      if (err) {
        console.log(err);
        return res.status(500).send(err.message);
      }

      passport.authenticate("local")(req, res, () => {
        const userPayload = {
          id: user.id,
          username: user.username,
          email: user.email,
        };

        const token = jwt.sign(userPayload, process.env.SECRET, {
          expiresIn: "1h",
        });
        res.cookie("jwt", token, { httpOnly: true });
        res
          .status(200)
          .json({ message: "Registered successfully", data: user });
      });
    });
  } catch (err) {
    res.status(500).json({ message: err });
  }
});

auth.post("/login", passport.authenticate("local"), async (req, res) => {
  const userData = req.body;

  try {
    const user = await User.findOne({ username: userData.username });

    if (!user) {
      return res.status(401).json({ message: "User not found" });
    }

    const userPayload = {
      id: user.id,
      username: user.username,
      email: user.email,
    };

    const token = jwt.sign(userPayload, process.env.SECRET, {
      expiresIn: "24h",
    });

    res
      .status(200)
      .cookie("jwt", token, { httpOnly: true })
      .json({ message: "Logged in successfully", user: user });
  } catch {
    res.status(500).json({ message: "An error occured" });
  }
});

auth.post("/logout", (req, res, next) => {
  req.logout((err) => {
    if (err) {
      return next(err);
    }
    res.cookie("jwt", "", {
      expires: new Date(0),
      httpOnly: true,
    });
    res.send("Logged out");
  });
});

auth.get("/verify-token", authenticateToken, (req, res) => {
  res.status(200).json({ user: req.user });
});

export default auth;
