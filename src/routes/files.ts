import express from "express";
import path from "path";
import fs from "fs";
import { __uploads } from "../utils/helpers.js";

const files = express.Router();

files.get("/avatars/:username", (req, res) => {
  const username = req.params.username;
  const avatarPath = path.join(__uploads, username, "avatar.svg");

  if (fs.existsSync(avatarPath)) {
    res.sendFile(avatarPath);
  } else {
    res.status(404).send("Avatar not found");
  }
});

export default files;
