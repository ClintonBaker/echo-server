import path from "path";
import { fileURLToPath } from "url";
import jwt from "jsonwebtoken";

const __filename = fileURLToPath(import.meta.url);
export const __dirname = path.dirname(__filename);
console.log(__dirname);
export const __uploads = path.join(__dirname, "..", "..", "..", "uploads");

export const verifyToken = (token) => {
  try {
    const decodedUser = jwt.verify(token, process.env.SECRET);
    return { isValid: true, user: decodedUser };
  } catch (error: any) {
    return { isValid: false, user: null };
  }
};
