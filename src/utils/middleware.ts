import { verifyToken } from "./helpers.js";

const authenticateToken = (req, res, next) => {
  const token = req.cookies.jwt;

  if (token === null) return res.sendStatus(401);

  const { isValid, user } = verifyToken(token);

  if (!isValid) return res.sendStatus(403);

  req.user = user;
  next();
};

export default authenticateToken;
