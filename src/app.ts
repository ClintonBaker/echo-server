import "dotenv/config";
import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import express from "express";
import cookieParser from "cookie-parser";
import mongoose from "mongoose";
import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import session from "express-session";
import cors from "cors";
import bodyParser from "body-parser";

import auth from "./routes/auth.js";
import User, { UserI } from "./models/User.js";
import files from "./routes/files.js";
import { typeDefs } from "./gql/schemas/index.js";
import { resolvers } from "./gql/resolvers/index.js";
import { verifyToken } from "./utils/helpers.js";
import DataLoader from "dataloader";
import Comment from "./models/Comment.js";
import { batchEntities } from "./gql/loaders/batchloader.js";

interface ApolloContext {
  user?: UserI;
}

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(cookieParser());

if (typeof process.env.MONGODB_URI === "undefined") {
  throw new Error("MONGODB_URI is not defined");
}

mongoose.connect(process.env.MONGODB_URI);
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  console.log("Connected to MongoDB");
});

app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());

const origin = ["http://localhost:3000"];
process.env.NODE_ENV === "dev" &&
  origin.push("https://studio.apollographql.com");

app.use(
  cors({
    origin: origin,
    credentials: true,
  })
);

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use("/", auth);
app.use("/", files);

const gqlServer = new ApolloServer<ApolloContext>({ typeDefs, resolvers });

await gqlServer.start();

app.use(
  "/graphql",
  express.json(),
  expressMiddleware(gqlServer, {
    context: async ({ req }) => {
      const token =
        req.cookies.jwt || req.headers.authorization?.split(" ")[1] || "";
      const { isValid, user } = verifyToken(token);
      return {
        user: isValid ? user : null,
        userLoader: new DataLoader((keys) => batchEntities(User, keys)),
        commentsLoader: new DataLoader((keys) =>
          batchEntities(Comment, keys, { createdAt: -1 })
        ),
      };
    },
  })
);

app.listen(port, () => console.log(`Server running on port ${port}!`));
