import mongoose, { Document, Model } from "mongoose";
import Comment from "./Comment.js";
import Reblog from "./Reblog.js";

export interface PostI extends Document {
  title: string;
  text: string;
  tags: string[];
  comments: mongoose.Types.ObjectId[];
  reblogs: mongoose.Types.ObjectId[];
  likes: mongoose.Types.ObjectId[];
  author: mongoose.Types.ObjectId;
}

const PostSchema = new mongoose.Schema(
  {
    title: String,
    text: String,
    tags: [String],
    comments: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comment",
        default: [],
      },
    ],
    reblogs: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Reblog",
        default: [],
      },
    ],
    likes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: [],
      },
    ],
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

PostSchema.pre(
  "deleteOne",
  { document: true, query: false },
  async function () {
    const doc = this;
    await Comment.deleteMany({ post: doc._id });
    await Reblog.deleteMany({ originalPost: doc._id });
  }
);

PostSchema.pre("deleteMany", async function () {
  const query = this.getQuery();
  await Comment.deleteMany({ post: { $in: query._id } });
  await Reblog.deleteMany({ originalPost: { $in: query._id } });
});

const Post: Model<PostI> = mongoose.model<PostI>("Post", PostSchema);
export default Post;
