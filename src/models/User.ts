import mongoose, { Document, Model } from "mongoose";
import passportLocalMongoose from "passport-local-mongoose";

export interface UserI extends Document {
  username: string;
  email: string;
  createdDate: Date;
  posts: mongoose.Types.ObjectId[];
  reblogs: mongoose.Types.ObjectId[];
  comments: mongoose.Types.ObjectId[];
  likedPosts: mongoose.Types.ObjectId[];
  likedComments: mongoose.Types.ObjectId[];
}

export interface NewUserI {
  username: string;
  email: string;
}

interface UserModel extends Model<UserI> {
  register(
    user: NewUserI,
    password: string,
    callback: (err: any, user: UserI) => void
  ): void;
  authenticate(): void;
  serializeUser(): void;
  deserializeUser(): void;
}

const UserSchema = new mongoose.Schema({
  createdDate: {
    type: Date,
    default: new Date(),
  },
  email: String,
  posts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      default: [],
    },
  ],
  reblogs: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Reblog",
      default: [],
    },
  ],
  comments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment",
      default: [],
    },
  ],
  likedPosts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      default: [],
    },
  ],
  likedComments: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment",
      default: [],
    },
  ],
});

UserSchema.plugin(passportLocalMongoose);

const User: UserModel = mongoose.model<UserI, UserModel>("User", UserSchema);

export default User;
