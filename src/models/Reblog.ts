import mongoose, { Document, Model } from "mongoose";

export interface ReblogI extends Document {
  originalPost: mongoose.Types.ObjectId;
  addedText: string;
  addedTags: string[];
  author: mongoose.Types.ObjectId;
}

const ReblogSchema = new mongoose.Schema(
  {
    originalPost: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      required: true,
    },
    addedText: String,
    addedTags: [String],
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  { timestamps: true }
);

const Reblog: Model<ReblogI> = mongoose.model<ReblogI>("Reblog", ReblogSchema);
export default Reblog;
