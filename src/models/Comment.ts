import mongoose, { Document, Model, ObjectId } from "mongoose";
import Post from "./Post.js";
import User from "./User.js";

export interface CommentI extends Document {
  text: string;
  author: mongoose.Types.ObjectId;
  post: mongoose.Types.ObjectId;
  parentComment: mongoose.Types.ObjectId | null;
  likes: mongoose.Types.ObjectId[];
}

const CommentSchema = new mongoose.Schema(
  {
    text: {
      type: String,
      required: true,
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    post: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      required: true,
    },
    parentComment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment",
      default: null,
    },
    likes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: [],
      },
    ],
  },
  { timestamps: true }
);

CommentSchema.pre(
  "deleteOne",
  { document: true, query: false },
  async function () {
    const doc = this;

    await Comment.deleteMany({ parentComment: doc._id });
    await Post.findByIdAndUpdate(doc.post, { $pull: { comments: doc._id } });
    await User.findByIdAndUpdate(doc.author, { $pull: { comments: doc._id } });
  }
);

CommentSchema.pre("deleteMany", async function () {
  const query = this.getQuery();

  const affectedComments = await Comment.find(query).select("_id post author");

  const postUpdates: Record<string, ObjectId[]> = {};
  const userUpdates: Record<string, ObjectId[]> = {};
  for (const comment of affectedComments) {
    const postId = comment.post?.toString();
    const userId = comment.post?.toString();

    if (postId) {
      postUpdates[postId] = postUpdates[postId] || [];
      postUpdates[postId].push(comment._id);
    }
    if (userId) {
      userUpdates[userId] = userUpdates[userId] || [];
      userUpdates[userId].push(comment._id);
    }
  }

  for (const [postId, commentIds] of Object.entries(postUpdates)) {
    await Post.findByIdAndUpdate(postId, {
      $pull: { comments: { $in: commentIds } },
    });
  }

  for (const [userId, commentIds] of Object.entries(userUpdates)) {
    await User.findByIdAndUpdate(userId, {
      $pull: { comments: { $in: commentIds } },
    });
  }
});

const Comment: Model<CommentI> = mongoose.model<CommentI>(
  "Comment",
  CommentSchema
);
export default Comment;
