import { UserI } from "./src/models/User.js";

declare global {
  namespace Express {
    interface Request {
      logout(callback: (err) => void): void;
      user: UserI;
    }
    interface Response {
      user: UserI;
    }
  }
}
